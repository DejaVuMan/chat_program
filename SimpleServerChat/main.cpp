#include <iostream>
#include <WS2tcpip.h> // WinSock is what windows uses to access networks
#include <string>
#include <sstream>
#include <vector>

#pragma comment (lib, "WS2_32.lib")

using namespace std;

void sender(fd_set master, SOCKET listening, SOCKET sock, char *buf)
{

	string userpart = "";
	string msgpart = "";
	int counter = 0; // save position so we can start from it later
	for (counter; counter < 25; counter++)
	{
		if (buf[counter] != ':') // -> change to '|' later
		{
			userpart = userpart + buf[counter];
		}
		else
		{
			counter++; // skip over delimiter
			break;
		}
	}

	while (buf[counter] != 0) // go to end buffer array - > change to '|' later
	{
		msgpart = msgpart + buf[counter];
		counter++;
	}
	/*
	later on, after AES encryption, we might want to do
	while(buf[counter] != 0
	{
		key = key + buf[counter];
		counter++
	}
	*/

	for (int i = 0; i < master.fd_count; i++)
	{
		SOCKET outSock = master.fd_array[i]; // no one outsockets the hut

		if (outSock == listening)
		{
			continue;
		}

		ostringstream ss; // we could potentially add a vector to facilitate the changing of a user's name

		if (outSock != sock) // if not listening sock and the same socket as the one we recieved from and user disconnect flag is false
		{
			if (msgpart != "has disconnected.")
			{
				ss << userpart << ": " << msgpart << "\r\n";
				// Output the socket number, what is sent, and then send to other clients
			}
			else if (userpart == "default:")
			{
				ss << "SOCKET # " << sock <<": " << msgpart << "\r\n";
			}
			else
			{
				ss << userpart << " " << msgpart << "\r\n";
			}
		}
		else
		{
				ss << "ME" << ": " << msgpart << "\r\n";
				// We could potentially do this client side, but its better to assume the client program is "dumb"
		}

		string strOut = ss.str();
		send(outSock, strOut.c_str(), strOut.size() + 1, 0);
	}
}

int main()
{
	//Start Winsock

	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2); // version 2.2

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		cerr << "Initializing WinSock failed. Ending program..." << endl;
		return 1;
	}

	//Create a socket

	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		cerr << "Failed to initialize socket. Ending program..." << endl;
		return 2;
	}
	
	//Bind socket to an IP Address + port

	sockaddr_in hint{};
	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000); // Port 54,000
	hint.sin_addr.S_un.S_addr = INADDR_ANY; // We could also use inet_pton - loop back address
	// htons = Host to Network Short
	// PC's are "little-endian," networking is "big-endian" - to combat this, we call on a series of functions

	bind(listening, (sockaddr*)&hint, sizeof(hint)); // address of pointer hint passed via pointer sockaddr
						
	//Tell Winsock to use the port for listening

	listen(listening, SOMAXCONN); // marks for listening - Max Size as backlog is 0x7fffffff AKA 2147483647 in decimal

	fd_set master{};
	FD_ZERO(&master); // ensure nothing exists inside the set

	FD_SET(listening, &master); // add socket to master

	while (true)
	{
		// we want to make a copy of the master file descriptor set - every time we call select, it destroys the file descriptor set
		fd_set copy = master;

		int socketCount = select(0, &copy, nullptr, nullptr, nullptr); 
		/* readfds is what we're only interested in - select() will wait if necessary to perform sync I/O
		Basically a queue - with multiple threads there would be less waiting, but threading introduces numerous complications  */

		// because the sockets are now essentially in a list, we can enumerate through them
		for (int i = 0; i < socketCount; i++)
		{
			// for now, the only thing our server can do is accept incoming request and recieve a message from client
			SOCKET sock = copy.fd_array[i];
			if (sock == listening)
			{
				// Accept connection if it is listening socket
				SOCKET client = accept(listening, nullptr, nullptr); // optionally, we can later on get client information

				// Add new connection to our master file set
				FD_SET(client, &master);

				// Send welcome message
				string welcome = "SERVER: Welcome to this multi-client testing server - now with a C# Client available!\r\n";
				send(client, welcome.c_str(), welcome.size() + 1, 0);
			}
			else
			{
				char buf[4096];
				ZeroMemory(buf, 4096);

				// Recieve message
				int bytesIn = recv(sock, buf, 4096, 0);
				if (bytesIn <= 0)
				{
					sender(master, listening, sock, buf);
					// Drop Client
					closesocket(sock);
					// sender(master, listening, sock, buf); 
					// TODO: make it send a message letting everyone know you disconnected
					FD_CLR(sock, &master);
				}
				else
				{
					//Send message to other clients, NOT to listening socket
					sender(master, listening, sock, buf);
				}
			}
		}
	}
	//Cleanup Winsock
	WSACleanup();
}